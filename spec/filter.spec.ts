import Filter from '../src/filter';
import { assert } from 'chai';
import { DBColumn, DBTable } from 'nodenamo';

describe('Filter', async () => {

    @DBTable({name: 'name'})
    class SomeClass {
        @DBColumn({id: true})
        id:string;
        @DBColumn({hash: true})
        key0: string;
        @DBColumn()
        key1: string;
        @DBColumn()
        key2: number;
        @DBColumn()
        key3: string[];
        @DBColumn()
        key4: boolean;
        key5: string;

        @DBColumn({range: true})
        rangeKey: string;
    }

    it('default filter', async () => {
        let filter = new Filter<SomeClass>(SomeClass).compare("key1", [ "value1", "value2" ] );

        let expression = filter.filter;
        assert.deepEqual(
            expression.expressionAttributeNames,
            {
                '#key1': 'key1'
            }
        )
        assert.deepEqual(
            expression.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2'
            }
        )
        assert.equal( expression.filterExpression, '( #key1 = :key1#0 or #key1 = :key1#1 )' )

        let expression2 = filter.compare("key4", true ).filter;

        assert.deepEqual(
            expression2.expressionAttributeNames,
            {
                '#key1': 'key1',
                '#key4': 'key4'
            }
        )
        assert.deepEqual(
            expression2.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2',
                ':key4#0': true
            }
        )
        assert.equal( expression2.filterExpression, '( #key1 = :key1#0 or #key1 = :key1#1 ) and #key4 = :key4#0' )
    })

    it('filter - exists', async () => {
        let filter = new Filter<SomeClass>(SomeClass);
        filter.compare("key1", [ "value1", "value2" ] );

        let expression = filter.filter;
        assert.deepEqual(
            expression.expressionAttributeNames,
            {
                '#key1': 'key1'
            }
        )
        assert.deepEqual(
            expression.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2'
            }
        )
        assert.equal( expression.filterExpression, '( #key1 = :key1#0 or #key1 = :key1#1 )' )

        filter.compare("key4", true );

        let expression2 = filter.filter;
        assert.deepEqual(
            expression2.expressionAttributeNames,
            {
                '#key1': 'key1',
                '#key4': 'key4'
            }
        )
        assert.deepEqual(
            expression2.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2',
                ':key4#0': true
            }
        )
        assert.equal( expression2.filterExpression, '( #key1 = :key1#0 or #key1 = :key1#1 ) and #key4 = :key4#0' )

        filter.attributeExists("key2");
        let expression3 = filter.filter;
        assert.deepEqual(
            expression3.expressionAttributeNames,
            {
                '#key1': 'key1',
                '#key4': 'key4'
            }
        )
        assert.deepEqual(
            expression3.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2',
                ':key4#0': true
            }
        )
        assert.equal( expression3.filterExpression, '( ( #key1 = :key1#0 or #key1 = :key1#1 ) and #key4 = :key4#0 ) and attribute_exists(key2)' )
    })

    it('default filter with and', async () => {
        let expression = new Filter<SomeClass>(SomeClass).compare("key1", [ "value1", "value2" ]).filter;

        assert.equal( expression.expressionAttributeNames['#key1'], 'key1' );
        assert.equal( expression.expressionAttributeValues[':key1#0'], 'value1' );
        assert.equal( expression.expressionAttributeValues[':key1#1'], 'value2' );
        assert.equal( expression.filterExpression, '( #key1 = :key1#0 or #key1 = :key1#1 )' )

        let expression2 = new Filter<SomeClass>(SomeClass).compare("key1", [ "value1", "value2" ]).and().compare("key4", true ).filter;
        assert.equal( expression2.expressionAttributeNames['#key1'], 'key1' );
        assert.equal( expression2.expressionAttributeNames['#key4'], 'key4' );
        assert.isTrue( expression2.expressionAttributeValues[':key4#0'] );
        assert.equal( expression2.expressionAttributeValues[':key1#0'], 'value1' );
        assert.equal( expression2.expressionAttributeValues[':key1#1'], 'value2' );
        assert.equal( expression2.filterExpression, '( #key1 = :key1#0 or #key1 = :key1#1 ) and #key4 = :key4#0' )
    })

    it('filter undefined', async () => {
        let expression = new Filter<SomeClass>(SomeClass).compare('key1', undefined).filter;
        assert.equal( expression.filterExpression, '')
        assert.isEmpty( expression.expressionAttributeNames );
        assert.isEmpty( expression.expressionAttributeValues );
        expression = new Filter<SomeClass>(SomeClass).compare('key1', [undefined]).filter;
        assert.equal( expression.filterExpression, '')
        assert.isEmpty( expression.expressionAttributeNames );
        assert.isEmpty( expression.expressionAttributeValues );
    })

    it('filter not column - throws', async () => {
        try
        {
            new Filter<SomeClass>(SomeClass).compare('key5', 'value4').filter;
            assert.isTrue(false);
        }
        catch(e)
        {
            assert.equal( e.message, "The column 'key5' does not exist on the table 'name'." )
        }
    })

    it('filter a hash - throws', async () => {
        try
        {
            new Filter<SomeClass>(SomeClass).compare('key0', 'value0').filter;
            assert.isTrue(false);
        }
        catch(e)
        {
            assert.equal( e.message, "The column 'key0' can not be used as a filter key because it is a hash key." )
        }
    })

    it('filter contains', async () => {
        let expression = new Filter<SomeClass>(SomeClass).contains('key1', 'value1').filter;
        assert.deepEqual(
            expression.expressionAttributeNames,
            {
                '#key1': 'key1'
            }
        )
        assert.deepEqual(
            expression.expressionAttributeValues,
            {
                ':key1#0': 'value1'
            }
        )
        assert.equal(expression.filterExpression, 'contains( #key1, :key1#0 )')
        let expression2 = new Filter<SomeClass>(SomeClass).contains('key1', [ 'value1', 'value2' ]).filter;
        assert.deepEqual(
            expression2.expressionAttributeNames,
            {
                '#key1': 'key1'
            }
        )
        assert.deepEqual(
            expression2.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2',
            }
        )
        assert.equal(expression2.filterExpression, '( contains( #key1, :key1#0 ) or contains( #key1, :key1#1 ) )')
    })

    it('filter beginsWith', async () => {
        let expression = new Filter(SomeClass).beginsWith('key1','value1').filter;
        assert.deepEqual(
            expression.expressionAttributeNames,
            {
                '#key1': 'key1'
            }
        )
        assert.deepEqual(
            expression.expressionAttributeValues,
            {
                ':key1#0': 'value1'
            }
        )
        assert.equal(expression.filterExpression, 'begins_with( #key1, :key1#0 )')
    })

    it('filter beginsWith multiple', async () => {
        let expression = new Filter(SomeClass).beginsWith('key1',['value1', 'value2']).filter;
        assert.deepEqual(
            expression.expressionAttributeNames,
            {
                '#key1': 'key1'
            }
        )
        assert.deepEqual(
            expression.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2'
            }
        )
        assert.equal(expression.filterExpression, '( begins_with( #key1, :key1#0 ) or begins_with( #key1, :key1#1 ) )')
    })

    it('filter with or make sure resets', async () => {
        let expression = new Filter(SomeClass).beginsWith('key1',['value1', 'value2']).or().contains('key1', 1).contains('key2', 2).filter;
        assert.deepEqual(
            expression.expressionAttributeNames,
            {
                '#key1': 'key1',
                '#key2': 'key2'
            }
        )
        assert.deepEqual(
            expression.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2',
                ':key1#2': 1,
                ':key2#0': 2
            }
        )
        assert.equal(expression.filterExpression, '( ( begins_with( #key1, :key1#0 ) or begins_with( #key1, :key1#1 ) ) or contains( #key1, :key1#2 ) ) and contains( #key2, :key2#0 )')
    })

    it('filter multiples same key', async () => {
        let expression = new Filter(SomeClass).or().contains('key1',['value1', 'value2']).contains('key1',['value3', 'value4']).contains('key1', 5 ).or().filter;
        assert.deepEqual(
            expression.expressionAttributeNames,
            {
                '#key1': 'key1'
            }
        )
        assert.deepEqual(
            expression.expressionAttributeValues,
            {
                ':key1#0': 'value1',
                ':key1#1': 'value2',
                ':key1#2': 'value3',
                ':key1#3': 'value4',
                ':key1#4': 5,
            }
        )
        assert.equal(expression.filterExpression, '( contains( #key1, :key1#0 ) or contains( #key1, :key1#1 ) ) and ( contains( #key1, :key1#2 ) or contains( #key1, :key1#3 ) ) and contains( #key1, :key1#4 )');
    })
})