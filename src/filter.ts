import { Reflector } from 'nodenamo/dist/reflector'

export enum JoinType {
    AND = 'and',
    OR = 'or'
}

export type ExpressionAttributeValues = { [ key: string ] : string | boolean | number };
export type ExpressionAttributeNames = { [ key: string ] : string };

export type ValidAttributeValueType = number | boolean | string | number[] | string[];

export default class Filter<T extends object>
{
    private filterExpressions: ( string | JoinType )[] = [];
    private expressionAttributeNames: ExpressionAttributeNames = {};
    private expressionAttributeValues: ExpressionAttributeValues = {};
    private idColumn: string;
    private tableColumns: string[];
    private hashColumns: string[];
    private rangeColumns: string[];
    private tableName: string;

    private validFilterValue( value: ValidAttributeValueType ): boolean
    {
        return ( value === undefined
            || ( Array.isArray(value) && value.some( v => v === undefined ) )
            ||  ( Array.isArray(value) && value.length === 0 ));
    }

    private validateFilterColumn( column: string ): void
    {
        if ( this.hashColumns.includes(column) )
        {
            throw new Error(`The column '${column}' can not be used as a filter key because it is a hash key.`);
        }
        if ( !this.tableColumns.includes(column) )
        {
            throw new Error(`The column '${column}' does not exist on the table '${this.tableName}'.`);
        }
    }

    private attributeValueString( prop: keyof T )
    {
        let valueString = `:${prop}`;
        let index = 0;
        do
        {
            valueString = `:${prop}#${index}`;
            if ( this.expressionAttributeValues[valueString] !== undefined )
            {
                index++;
            }
        }
        while ( this.expressionAttributeValues[valueString] !== undefined )
        return valueString;
    }

    contains( prop: keyof T, value: ValidAttributeValueType ): Filter<T>
    {
        if ( this.validFilterValue(value) )
        {
            return this;
        }
        this.validateFilterColumn(prop as string);
        let values = Array.isArray(value)? value: [ value ];
        this.expressionAttributeNames[`#${prop}`] = prop as string;
        let index = 0;
        let f = [];
        for ( const val of values )
        {
            let valueString = this.attributeValueString(prop);
            this.expressionAttributeValues[valueString] = val;
            f.push(`contains( #${prop}, ${valueString} )`)
            index++;
        }
        this.filterExpressions.push( f.length > 1? `( ${f.join(' or ')} )`: f[0] );

        return this;
    }

    compare( prop: keyof T, value: ValidAttributeValueType ): Filter<T>
    {
        if ( this.validFilterValue(value) )
        {
            return this;
        }
        this.validateFilterColumn(prop as string);
        let values = Array.isArray(value)? value: [ value ];
        this.expressionAttributeNames[`#${prop}`] = prop as string;
        let index = 0;
        let f = [];
        for ( const val of values )
        {
            let valueString = this.attributeValueString(prop);
            this.expressionAttributeValues[valueString] = val;
            f.push(`#${prop} = ${valueString}`)
            index++;
        }
        this.filterExpressions.push( f.length > 1? `( ${f.join(' or ')} )`: f[0] );
        return this;
    }

    beginsWith( prop: keyof T, value: ValidAttributeValueType ): Filter<T>
    {
        this.validateFilterColumn(prop as string);
        let values = Array.isArray(value)? value: [ value ];
        this.expressionAttributeNames[`#${prop}`] = prop as string;
        let index = 0;
        let f = [];
        for ( const val of values )
        {
            let valueString = this.attributeValueString(prop);
            this.expressionAttributeValues[valueString] = val;
            f.push(`begins_with( #${prop}, ${valueString} )`)
            index++;
        }
        this.filterExpressions.push( f.length > 1? `( ${f.join(' or ')} )`: f[0] );
        return this;
    }

    attributeExists( prop: keyof T ): Filter<T>
    {
        this.validateFilterColumn(prop as string);
        this.filterExpressions.push(`attribute_exists(${prop})`)
        return this;
    }

    attributeNotExists( prop: keyof T ): Filter<T>
    {
        this.validateFilterColumn(prop as string);
        this.filterExpressions.push(`attribute_not_exists(${prop})`)
        return this;
    }

    and(): Filter<T>
    {
        if ( this.filterExpressions.length > 0 )
        {
            this.filterExpressions.push(JoinType.AND);
        }
        return this;
    }

    or(): Filter<T>
    {
        if ( this.filterExpressions.length > 0 )
        {
            this.filterExpressions.push(JoinType.OR);
        }
        return this;
    }

    get filter(): { expressionAttributeNames: ExpressionAttributeNames, expressionAttributeValues: ExpressionAttributeValues, filterExpression: string }
    {
        let fEx = '';
        let joinType = JoinType.AND;

        let filterExpressionsCopy = JSON.parse(JSON.stringify(this.filterExpressions));

        if ( filterExpressionsCopy[0] === JoinType.AND || filterExpressionsCopy[0] === JoinType.OR )
        {
            filterExpressionsCopy.shift();
        }

        if ( filterExpressionsCopy[filterExpressionsCopy.length - 1] === JoinType.AND || filterExpressionsCopy[filterExpressionsCopy.length - 1] === JoinType.OR )
        {
            filterExpressionsCopy.pop();
        }
        let remaining = filterExpressionsCopy.length;

        for ( const filter of filterExpressionsCopy )
        {
            if ( filter === JoinType.OR || filter === JoinType.AND )
            {
                joinType = filter;
            }
            else
            {
                if ( remaining > 1 && !filter.startsWith('(') )
                {
                    fEx = ( fEx.length > 0? [ '(', fEx, joinType, filter, ')' ]: [ filter ]).join(' ');
                }
                else
                {
                    fEx = ( fEx.length > 0? [ fEx, joinType, filter ]: [ filter ]).join(' ');
                }
                joinType = JoinType.AND;
            }
            remaining--;
        }

        return {
            expressionAttributeNames: this.expressionAttributeNames,
            expressionAttributeValues: this.expressionAttributeValues,
            filterExpression: fEx
        };
    }

    constructor( type: { new ( ...args: any[] ): T } )
    {
        let obj:T = new type();
        this.idColumn = Reflector.getIdKey(obj)
        this.hashColumns = Reflector.getAllHashKeys(obj);
        this.rangeColumns = Reflector.getAllRangeKeys(obj);
        this.tableColumns = Reflector.getColumns(obj);
        this.tableName = Reflector.getTableName(obj);
    }
}